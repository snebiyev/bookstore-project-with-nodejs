const express = require('express');
const cors = require('cors');
const app = express();

// Middleware to parse incoming requests with JSON payloads
app.use(express.json());

// Middleware to parse incoming requests with URL-encoded payloads
app.use(express.urlencoded({ extended: true }));

// Middleware to handle CORS
app.use(cors());

app.get('/', (req, res, next) => {
  res.status(200).json({
    message: 'Server is running!',
  });
});

// Error handling middleware
class HttpError extends Error {
  constructor(message, status) {
    super(message);
    this.status = status;
  }
}

app.use((req, res, next) => {
  const error = new HttpError('Resource not found', 404);
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    error: {
      message: error.message,
      status: error.status,
    },
  });
});

module.exports = app;
